var gulp = require('gulp');
var nuget = require('gulp-nuget');

// we want to force the release parameter when creating packages
// todo: need to find a better way of doing this
// process.argv.push('--release');

gulp.task('package', ['bump', 'rebuild', 'get-nuget'], function(){
    return gulp.src("*.nuspec")
    .pipe(nuget.pack({nuget: 'nuget.exe'}))
    .pipe(gulp.dest('C:/Source/Packages/'));
});