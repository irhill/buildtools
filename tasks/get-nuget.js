var gulp = require('gulp');
var request = require('request');
var fs = require('fs');

gulp.task('get-nuget', function(done){
    if(fs.existsSync('nuget.exe')){
        return done();
    }

    request.get('http://nuget.org/nuget.exe')
    .pipe(fs.createWriteStream('nuget.exe'))
    .on('close', done);
})