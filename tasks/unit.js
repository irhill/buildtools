var gulp = require('gulp');
var xunit = require('gulp-xunit-runner');

gulp.task('unit', ['rebuild'], function() {
    var releaseParam = process.argv.indexOf('--release');
    var srcDir = releaseParam > -1 ? 'Release' : 'Debug';
    var srcMatcher = '**/bin/' + srcDir + '/*.Tests.Unit.dll'; 

    return gulp.src(srcMatcher, {read: false})
    .pipe(xunit({
         executable: 'C:/source/bitbucket/Magic8Ball.Client/packages/xunit.runner.console.2.2.0/tools/xunit.console.exe'
    }));
});