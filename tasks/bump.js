var gulp = require('gulp');
var bump = require('gulp-bump');

gulp.task('bump', function(){
    var majorBump = process.argv.indexOf('--major');
    var minorBump = process.argv.indexOf('--minor');

    var bumpType = majorBump > -1 ? 'major' : minorBump > -1 ? 'minor' : 'patch'

    gulp.src('*.nuspec')
    .pipe(bump({type: bumpType}))
    .pipe(gulp.dest('./'));
});