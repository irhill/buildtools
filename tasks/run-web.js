var gulp = require('gulp');
var cheerio = require('gulp-cheerio');
var filepath = require('path');
var spawn = require('child_process').spawn;

return gulp.task('run-web', function(){
    var releaseParam = process.argv.indexOf('--release');
    var buildConfiguration = releaseParam > -1 ? 'bin/release' : 'bin/debug';

    return gulp.src('**/*Web*.csproj')
    .pipe(cheerio({
        parserOptions: {
            xmlMode: true
        },
        run: function($, file, done) {
            if($('OutputType').text() === 'Exe') {
                var options = {
                    shell: true,
                    detached: true
                };

                var assemblyName = $('AssemblyName').text();
                var assemblyPath = filepath.dirname(file.path);
                var commandToExecute = filepath.join(assemblyPath, buildConfiguration, assemblyName) + '.exe';

                console.log('starting ' + commandToExecute);
                spawn(commandToExecute, options);
            }

            done();
        }
    }));
});