var gulp = require('gulp');
var msbuild = require('gulp-msbuild');

gulp.task('rebuild', ['clean'], function() {
    var releaseParam = process.argv.indexOf('--release');

    var properties = {
        configuration: releaseParam > -1 ? 'Release' : 'Debug'
    };

    return gulp.src('*.sln')
        .pipe(msbuild({
            toolsVersion: 'auto',
            targets: ["Rebuild"],
            stdout: true,
            errorOnFail: true,
            properties: properties  
        }));
});